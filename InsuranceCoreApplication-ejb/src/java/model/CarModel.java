/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author konrad
 */
@Entity
@Table(name = "CAR_MODEL"//,
       //uniqueConstraints = @UniqueConstraint(columnNames={"NAME"})
)
@NamedQueries({
    @NamedQuery(name = "CarModel.findAll", query = "SELECT cm FROM CarModel cm"),
    @NamedQuery(name = "CarModel.findByNameAndManufactureYear", query = "SELECT cm FROM CarModel cm where cm.name = :name AND cm.manufactureYear = :manufactureYear")
})
public class CarModel implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    
    @NotNull @Size(min = 1, max = 100)
    @Basic(optional = false)
    @Column(name = "NAME")
    private String name;
    
    @NotNull
    @Basic(optional = false)
    @Column(name = "PRICE")
    private Float price;
    
    @NotNull
    @Basic(optional = false)
    @Column(name = "MANUFACTURE_YEAR")
    private Integer manufactureYear;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Float getPrice() {
        return price;
    }

    public void setPrice(Float price) {
        this.price = price;
    }

    public Integer getManufactureYear() {
        return manufactureYear;
    }

    public void setManufactureYear(Integer manufactureYear) {
        this.manufactureYear = manufactureYear;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CarModel)) {
            return false;
        }
        CarModel other = (CarModel) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "model.CarModel[ id=" + id + " ]";
    }
    
}
