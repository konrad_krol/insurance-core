/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author konrad
 */
@Entity
@Table(name = "CLIENT"//,
       //uniqueConstraints = @UniqueConstraint(columnNames={"PESEL"})
)
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Client.findAll", query = "SELECT c FROM Client c"),
    @NamedQuery(name = "Client.findById", query = "SELECT c FROM Client c WHERE c.id = :id"),
    @NamedQuery(name = "Client.findByPesel", query = "SELECT c FROM Client c WHERE c.pesel = :pesel")
})
public class Client implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id @GeneratedValue @NotNull
    @Basic(optional = false)
    @Column(name = "ID")
    private Integer id;
    
    @NotNull @Size(min = 1, max = 100)
    @Basic(optional = false)
    @Column(name = "FIRSTNAME")
    private String firstname;

    @NotNull @Size(min = 1, max = 100)
    @Basic(optional = false)
    @Column(name = "LASTNAME")
    private String lastname;
    
    @NotNull @Size(min = 1, max = 100)
    @Basic(optional = false)
    @Column(name = "ADDRESS")
    private String address;
    
    @NotNull @Size(min = 1, max = 100)
    @Basic(optional = false)
    @Column(name = "CITY")
    private String city;
    
    @NotNull @Size(min = 1, max = 100)
    @Basic(optional = false)
    @Column(name = "POSTCODE")
    private String postcode;
    
    @NotNull @Size(min = 1, max = 100)
    @Basic(optional = false)
    @Column(name = "PESEL")
    private String pesel;
    
    @NotNull @Size(min = 1, max = 100)
    @Basic(optional = false)
    @Column(name = "GENDER")
    private String gender;
    
    @NotNull
    @Basic(optional = false)
    @Temporal(TemporalType.DATE)
    @Column(name = "BIRTH_DATE")
    private Date birthDate;
    
    @Size(min = 0, max = 100)
    @Column(name = "LICENSE_NUMBER")
    private String licenseNumber;
    
    @NotNull
    @Basic(optional = false)
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "UPDATED_AT")
    private Date updatedAt;

    public Client() {
    }

    public Client(Integer id) {
        this.id = id;
    }

    public Client(Integer id, String firstname, String lastname) {
        this.id = id;
        this.firstname = firstname;
        this.lastname = lastname;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getLicenseNumber() {
        return licenseNumber;
    }

    public void setLicenseNumber(String licenseNumber) {
        this.licenseNumber = licenseNumber;
    }

    public String getPesel() {
        return pesel;
    }

    public void setPesel(String pesel) {
        this.pesel = pesel;
    }

    public String getPostcode() {
        return postcode;
    }

    public void setPostcode(String postcode) {
        this.postcode = postcode;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }
    
    
    

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Client)) {
            return false;
        }
        Client other = (Client) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "model.Client[ id=" + id + " ]";
    }
    
}
