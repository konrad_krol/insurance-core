/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.NoResultException;
import model.Car;
import model.CarModel;

/**
 *
 * @author konrad
 */
public class CarDao {
    EntityManagerFactory enf;
    
    public CarDao(EntityManagerFactory enf) {
        this.enf = enf;
    }
    
    public Car findCar(String vin) {    
        EntityManager en = enf.createEntityManager();
        Car car = null;
        try {
            car = (Car) en.createNamedQuery("Car.findByVin").setParameter("vin", vin).getSingleResult();
        } catch (NoResultException e) {
            car = null;
        }
        en.close();
        
        return car;
    }
}
