/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.io.Serializable;
import javax.persistence.*;
import model.Client;
import model.AdminUser;

/**
 *
 * @author konrad
 */
public class AdminUserDao {
    EntityManagerFactory enf;
    
    public AdminUserDao(EntityManagerFactory enf) {
        this.enf = enf;
    }
    
    public AdminUser findUser(String username) {
        EntityManager en = enf.createEntityManager();
        
        AdminUser user = null;
        try {
            user = (AdminUser) en.createNamedQuery("AdminUser.findByUsername")
                                    .setParameter("username", username)
                                    .getSingleResult();
        } catch (NoResultException e) {
            user = null;
        }
        en.close();
        return user;
    }
}
