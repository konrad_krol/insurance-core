/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceUnit;
import model.CarModel;

/**
 *
 * @author konrad
 */
public class CarModelDao {
    EntityManagerFactory enf;
    
    public CarModelDao(EntityManagerFactory enf) {
        this.enf = enf;
    }

    public CarModel findCarModel(String name, Integer manufactureYear) {    
        EntityManager en = enf.createEntityManager();
        
        CarModel carModel = null;
        try {
            carModel = (CarModel) en.createNamedQuery("CarModel.findByNameAndManufactureYear")
                                    .setParameter("name", name)
                                    .setParameter("manufactureYear", manufactureYear)
                                    .getSingleResult();
        } catch (NoResultException e) {
            carModel = null;
        }
        en.close();
        return carModel;
    }
}
