/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.NoResultException;
import model.CarModel;
import model.Client;

/**
 *
 * @author konrad
 */
public class ClientDao {
    EntityManagerFactory enf;
    
    public ClientDao(EntityManagerFactory enf) {
        this.enf = enf;
    }

    public Client findClient(String pesel) {    
        EntityManager en = enf.createEntityManager();
        
        Client client = null;
        try {
            client = (Client) en.createNamedQuery("Client.findByPesel")
                                    .setParameter("pesel", pesel)
                                    .getSingleResult();
        } catch (NoResultException e) {
            client = null;
        }
        en.close();
        return client;
    }
}
