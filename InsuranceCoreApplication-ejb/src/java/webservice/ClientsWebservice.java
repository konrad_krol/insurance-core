/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package webservice;

import java.util.Date;
import java.util.List;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceUnit;
import model.Client;

/**
 *
 * @author konrad
 */
@WebService(serviceName = "ClientsWebservice")
@Stateless()
public class ClientsWebservice {
    @PersistenceUnit
    EntityManagerFactory enf;

    @WebMethod(operationName = "findClientByPesel")
    public Client findClientByPesel(@WebParam(name = "pesel") String pesel) {
        
        EntityManager en = enf.createEntityManager();
        try {
            return (Client) en.createNamedQuery("Client.findByPesel")
                                       .setParameter("pesel", pesel).getSingleResult();
        }
        catch(NoResultException e) { return null; }
        finally { en.close(); } 
    }
    
    @WebMethod(operationName = "createClient")
    public Integer createClient(@WebParam(name = "firstName") String firstName, 
                                  @WebParam(name = "lastName") String lastName,
                                  @WebParam(name = "address") String address,
                                  @WebParam(name = "city") String city,
                                  @WebParam(name = "postcode") String postcode,
                                  @WebParam(name = "pesel") String pesel,
                                  @WebParam(name = "gender") String gender,
                                  @WebParam(name = "birthDate") Date birthDate,
                                  @WebParam(name = "licenseNumber") String licenseNumber,
                                  @WebParam(name = "updatedAt") Date updatedAt) {
        
        Client client = new Client();
        client.setFirstname(firstName);
        client.setLastname(lastName);
        client.setAddress(address);
        client.setCity(city);
        client.setPostcode(postcode);
        client.setPesel(pesel);
        client.setGender(gender);
        client.setBirthDate(birthDate);
        client.setLicenseNumber(licenseNumber);
        client.setUpdatedAt(updatedAt);
        
        EntityManager en = enf.createEntityManager();
        en.persist(client); 
        en.close();
        
        return client.getId();
    }
    
    @WebMethod(operationName = "updateClient")
    public Integer updateClient(@WebParam(name = "firstName") String firstName, 
                                  @WebParam(name = "lastName") String lastName,
                                  @WebParam(name = "address") String address,
                                  @WebParam(name = "city") String city,
                                  @WebParam(name = "postcode") String postcode,
                                  @WebParam(name = "pesel") String pesel,
                                  @WebParam(name = "gender") String gender,
                                  @WebParam(name = "birthDate") Date birthDate,
                                  @WebParam(name = "licenseNumber") String licenseNumber,
                                  @WebParam(name = "updatedAt") Date updatedAt) {
        
        EntityManager en = enf.createEntityManager();
        Client client = (Client) en.createNamedQuery("Client.findByPesel")
                                       .setParameter("pesel", pesel).getSingleResult();;
        client.setFirstname(firstName);
        client.setLastname(lastName);
        client.setAddress(address);
        client.setCity(city);
        client.setPostcode(postcode);
        client.setPesel(pesel);
        client.setGender(gender);
        client.setBirthDate(birthDate);
        client.setLicenseNumber(licenseNumber);
        client.setUpdatedAt(updatedAt);
        
        en.merge(client); 
        en.close();
        
        return client.getId();
    }
    
}
