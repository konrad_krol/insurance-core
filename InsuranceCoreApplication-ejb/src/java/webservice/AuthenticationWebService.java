/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package webservice;

import dao.AdminUserDao;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.ejb.Stateless;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import model.AdminUser;

/**
 *
 * @author konrad
 */
@WebService(serviceName = "AuthenticationWebService")
@Stateless()
public class AuthenticationWebService {
    @PersistenceUnit
    EntityManagerFactory enf;
    /**
     * This is a sample web service operation
     */
    @WebMethod(operationName = "authenticate")
    public boolean authenticate(@WebParam(name = "username") String username,
        @WebParam(name = "password") String password) {
        AdminUserDao userDao = new AdminUserDao(enf);
        AdminUser user = userDao.findUser(username);
        
        if(user != null && user.getPassword().equals(password)) {
            return true;
        }
        else {
            return false;
        }
    }
}
