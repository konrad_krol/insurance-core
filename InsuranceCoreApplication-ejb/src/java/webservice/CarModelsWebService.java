/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package webservice;

import java.util.List;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import model.CarModel;

/**
 *
 * @author konrad
 */
@WebService(serviceName = "CarModelsWebService")
@Stateless()
public class CarModelsWebService {
    @PersistenceUnit
    EntityManagerFactory enf;

    @WebMethod(operationName = "getAllCarModels")
    public List<CarModel> getAllCarModels() {
        EntityManager en = enf.createEntityManager();
        List<CarModel> result = en.createNamedQuery("CarModel.findAll").getResultList();
        en.close();
        return result;
    }
    
    @WebMethod(operationName = "createCarModel")
    public void createCarModel(@WebParam(name = "manufactureYear") Integer manufactureYear,
        @WebParam(name = "name") String name,
        @WebParam(name = "price") Float price) {
        
        EntityManager en = enf.createEntityManager();
        CarModel carModel = new CarModel();
        carModel.setManufactureYear(manufactureYear);
        carModel.setName(name);
        carModel.setPrice(price);
        
        en.persist(carModel);
        en.close(); 
    }
    
    @WebMethod(operationName = "updateCarModel")
    public void updateCarModel(@WebParam(name = "id") Integer id,
        @WebParam(name = "manufactureYear") Integer manufactureYear,
        @WebParam(name = "name") String name,
        @WebParam(name = "price") Float price) {
        
        EntityManager en = enf.createEntityManager();
        CarModel carModel = en.find(CarModel.class, id.longValue());
        carModel.setManufactureYear(manufactureYear);
        carModel.setName(name);
        carModel.setPrice(price);
        
        en.merge(carModel);
        en.close(); 
    }
    
    @WebMethod(operationName = "deleteCarModel")
    public void deleteCarModel(@WebParam(name = "id") Integer id) {
        
        EntityManager en = enf.createEntityManager();
        CarModel carModel = en.find(CarModel.class, id.longValue());
        en.remove(carModel);
        en.close(); 
    }
}
