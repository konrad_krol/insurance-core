/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package webservice;

import dao.CarModelDao;
import java.util.Date;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceUnit;
import model.Car;
import model.CarModel;

/**
 *
 * @author konrad
 */
@WebService(serviceName = "CarsWebService")
@Stateless()
public class CarsWebService {
    @PersistenceUnit
    EntityManagerFactory enf;

    @WebMethod(operationName = "findCarByVin")
    public Car findCarByVin(@WebParam(name = "vin") String vin) {
        EntityManager en = enf.createEntityManager();
        Car car = null;

        try {
            car = (Car) en.createNamedQuery("Car.findByVin").setParameter("vin", vin).getSingleResult();
        } catch (NoResultException e) {
            car = null;
        }

        en.close();
        return car;
    }

    @WebMethod(operationName = "createCar")
    public Integer createCar(@WebParam(name = "vin") String vin,
            @WebParam(name = "platesNumber") String platesNumber,
            @WebParam(name = "carModelName") String carModelName,
            @WebParam(name = "carModelManufactureYear") Integer carModelManufactureYear,
            @WebParam(name = "dateOfManufacture") Date dateOfManufacture,
            @WebParam(name = "updatedAt") Date updatedAt) {

        Car car = new Car();
        car.setPlatesNumber(platesNumber);
        car.setVin(vin);
        car.setDateOfManufacture(dateOfManufacture);
        car.setUpdatedAt(updatedAt);
        
        CarModelDao carModelDao = new CarModelDao(this.enf);
        car.setCarModel(carModelDao.findCarModel(carModelName, carModelManufactureYear));

        EntityManager en = enf.createEntityManager();
        en.persist(car);
        en.close();

        return car.getId();
    }

    @WebMethod(operationName = "updateCar")
    public Integer updateCar(@WebParam(name = "vin") String vin,
            @WebParam(name = "platesNumber") String platesNumber,
            @WebParam(name = "carModelName") String carModelName,
            @WebParam(name = "carModelManufactureYear") Integer carModelManufactureYear,
            @WebParam(name = "dateOfManufacture") Date dateOfManufacture,
            @WebParam(name = "updatedAt") Date updatedAt) {

        EntityManager en = enf.createEntityManager();
        Car car = (Car) en.createNamedQuery("Car.findByVin").setParameter("vin", vin).getSingleResult();;
        car.setPlatesNumber(platesNumber);
        car.setVin(vin);
        car.setDateOfManufacture(dateOfManufacture);
        car.setUpdatedAt(updatedAt);
        
        CarModelDao carModelDao = new CarModelDao(this.enf);
        car.setCarModel(carModelDao.findCarModel(carModelName, carModelManufactureYear));

        en.merge(car);
        en.close();

        return car.getId();
    }
    
    
}
