/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package webservice;

import dao.CarDao;
import dao.CarModelDao;
import dao.ClientDao;
import java.util.Date;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceUnit;
import model.Car;
import model.Client;
import model.Policy;

/**
 *
 * @author konrad
 */
@WebService(serviceName = "PolicyWebService")
@Stateless()
public class PolicyWebService {
    @PersistenceUnit
    EntityManagerFactory enf;

    @WebMethod(operationName = "findPolicyByPolicyNumber")
    public Policy findPolicyByPolicyNumber(@WebParam(name = "policyNumber") String policyNumber) {
        EntityManager en = enf.createEntityManager();
        Policy policy = null;

        try {
            policy = (Policy) en.createNamedQuery("Policy.findByPolicyNumber").setParameter("policyNumber", policyNumber).getSingleResult();
        } catch (NoResultException e) {
            policy = null;
        }

        en.close();
        return policy;
    }

    @WebMethod(operationName = "createPolicy")
    public Integer createPolicy(@WebParam(name = "policyNumber") String policyNumber,
            @WebParam(name = "totalPrice") Float totalPrice,
            @WebParam(name = "clientPesel") String clientPesel,
            @WebParam(name = "carVin") String carVin,
            @WebParam(name = "updatedAt") Date updatedAt) {

        Policy policy = new Policy();
        policy.setPolicyNumber(policyNumber);
        policy.setTotalPrice(totalPrice);
        policy.setUpdatedAt(updatedAt);
        
        CarDao carDao = new CarDao(enf);
        Car car = carDao.findCar(carVin);
        policy.setCar(car);
        
        ClientDao clientDao = new ClientDao(enf);
        Client client = clientDao.findClient(clientPesel);
        policy.setClient(client);

        EntityManager en = enf.createEntityManager();
        en.persist(policy);
        en.close();

        return policy.getId();
    }

    @WebMethod(operationName = "updatePolicy")
    public Integer updatePolicy(@WebParam(name = "policyNumber") String policyNumber,
            @WebParam(name = "totalPrice") Float totalPrice,
            @WebParam(name = "clientPesel") String clientPesel,
            @WebParam(name = "carVin") String carVin,
            @WebParam(name = "updatedAt") Date updatedAt) {

        EntityManager en = enf.createEntityManager();
        Policy policy = (Policy) en.createNamedQuery("Policy.findByPolicyNumber").setParameter("policyNumber", policyNumber).getSingleResult();;
        policy.setPolicyNumber(policyNumber);
        policy.setTotalPrice(totalPrice);
        policy.setUpdatedAt(updatedAt);
        
        CarDao carDao = new CarDao(enf);
        Car car = carDao.findCar(carVin);
        policy.setCar(car);
        
        ClientDao clientDao = new ClientDao(enf);
        Client client = clientDao.findClient(clientPesel);
        policy.setClient(client);

        en.merge(policy);
        en.close();

        return policy.getId();
    }
}
